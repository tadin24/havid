<?php

namespace App\Models;

use CodeIgniter\Model;

class MsReferensiModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'ms_reff';
    protected $primaryKey       = 'reff_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'reff_id',
        'reff_kode',
        'reff_status',
        'reff_nama',
        'reff_value',
        'refcat_id',
    ];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];


    public function get_total($where)
    {
        $sql = "SELECT
                    count(*) as total
                from
                    $this->table mr
                where
                    0 = 0
                    $where";
        return $this->db->query($sql)->getRow()->total;
    }

    public function get_data($limit, $where, $order, $columns)
    {
        $slc = implode(',', $columns);
        $sql = "SELECT
                    $slc
                from
                    $this->table mr
                where
                    0 = 0
                    $where
                $order $limit";
        $data = $this->db->query($sql)->getResult();;
        return $data;
    }
}
