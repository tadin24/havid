<?php

namespace App\Models;

use CodeIgniter\Model;

class DashboardModel extends Model
{
    public function get_total($where)
    {
        $sql = "SELECT
                    count(*) as total
                from
                    data_device dd
                left join ms_reff k on
                    k.refcat_id = 1
                    and k.reff_value = dd.output
                left join ms_reff g on
                    g.refcat_id = 2
                    and g.reff_value = dd.gangguan
                where
                    0 = 0
                    $where";
        return $this->db->query($sql)->getRow()->total;
    }

    public function get_data($limit, $where, $order)
    {
        $sql = "SELECT
                    dd.dd_id ,
                    dd.v1 ,
                    dd.v2 ,
                    dd.v3 ,
                    dd.timer ,
                    dd.created_at ,
                    dd.output ,
                    k.reff_nama as kondisi ,
                    case
                        when g.reff_nama is null then 'Tidak Ada Gangguan'
                        else g.reff_nama
                    end as gangguan ,
                    dd.kwh 
                from
                    data_device dd
                left join ms_reff k on
                    k.refcat_id = 1
                    and k.reff_value = dd.output
                left join ms_reff g on
                    g.refcat_id = 2
                    and g.reff_value = dd.gangguan
                where
                    0 = 0
                    $where
                $order $limit";
        return $this->db->query($sql)->getResult();
    }

    // ============================================================
    // if type get == 1
    public function get_label($where, $limit)
    {
        $sql = "SELECT
                    *
                from
                    (
                    select
                        dd.dd_id,
                        dd.created_at
                    from
                        data_device dd
                    where
                        0 = 0
                        $where
                    order by
                        dd_id desc
                    limit $limit) d
                order by
                    d.dd_id";
        // echo $sql;
        return $this->db->query($sql)->getResult();
    }

    public function get_sensor($where, $limit)
    {
        $sql = "SELECT
                    *
                from
                    (
                    select
                        dd.dd_id,
                        dd.v1,
                        dd.v2,
                        dd.v3,
                        dd.i1,
                        dd.i2,
                        dd.i3,
                        dd.created_at
                    from
                        data_device dd
                    where
                        0 = 0
                        $where
                    order by
                        dd_id desc
                    limit $limit) d
                order by
                    d.dd_id";
        return $this->db->query($sql)->getResult();
    }

    // ============================================================
}
