<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\DataDeviceModel;
use Config\Database;

class DataDevice extends BaseController
{
    protected $ld_id;
    protected $M_data_device;

    public function __construct()
    {
        $this->M_data_device = new DataDeviceModel();
    }

    public function check()
    {
        $device_kode = $this->request->getVar('device_kode');
        $db = Database::connect();
        $device = $db
            ->query(
                "SELECT
                    *
                from
                    list_device ld
                where
                    ld.ld_kode = '$device_kode'"
            );
        if ($device->getNumRows() <= 0) {
            $res['status'] = false;
            $res['data'] = $this->response->setStatusCode(403)->setJSON(['msg' => 'Device tidak ditemukan!']);
        } else {
            if ($device->getRow()->ld_status != 1) {
                $res['status'] = false;
                $res['data'] = $this->response->setStatusCode(403)->setJSON(['msg' => 'Device tidak aktif!']);
            } else {
                $this->ld_id = $device->getRow()->ld_id;
                $res['status'] = true;
            }
        }

        return $res;
    }

    public function store()
    {
        $res = $this->check();
        if (!$res['status']) {
            return $res['data'];
        }

        $data = [
            'ld_id'     => $this->ld_id,
            'v1'        => addslashes($this->request->getVar('v1')),
            'v2'        => addslashes($this->request->getVar('v2')),
            'v3'        => addslashes($this->request->getVar('v3')),
            'i1'        => addslashes($this->request->getVar('i1')),
            'i2'        => addslashes($this->request->getVar('i2')),
            'i3'        => addslashes($this->request->getVar('i3')),
            'kwh'       => addslashes($this->request->getVar('kwh')),
            'timer'     => addslashes($this->request->getVar('timer')),
            'output'    => addslashes($this->request->getVar('output')),
            'gangguan'  => addslashes($this->request->getVar('gangguan')),
        ];

        $res = $this->M_data_device->save($data);

        if ($res) {
            return $this->response->setStatusCode(200)->setJSON(['msg' => 'Data berhasil ditambahkan!']);
        } else {
            return $this->response->setStatusCode(400)->setJSON(['msg' => 'Data gagal ditambahkan!']);
        }
    }
}
