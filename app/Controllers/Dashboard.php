<?php

namespace App\Controllers;

use App\Controllers\MyController;
use App\Models\DashboardModel;
use App\Models\DataDeviceModel;
use App\Models\ListDeviceModel;
use App\Models\MsDeviceModel;

class Dashboard extends MyController
{
    protected $M_ms_device;
    protected $M_dashboard;
    protected $M_data_device;

    public function __construct()
    {
        parent::__construct();
        $this->M_ms_device = new MsDeviceModel();
        $this->M_dashboard = new DashboardModel();
        $this->M_data_device = new DataDeviceModel();
    }

    public function index()
    {
        $data['opt_ms_device'] = $this->M_ms_device
            ->where('device_status', 1)
            ->orderBy('device_kode', 'asc')
            ->find();
        $data['title'] = "Dashboard";
        return $this->base_theme('v_dashboard', $data);
    }

    public function get_device()
    {
        $device_id = $this->request->getVar('device_id');
        $M_list_device = new ListDeviceModel();
        $res = $M_list_device
            ->where('device_id', $device_id)
            ->where('ld_status', 1);
        if ($this->userdata->user_id != 1) {
            $res = $res->where('user_id', $this->userdata->user_id);
        }
        $res = $res
            ->orderBy('ld_kode', 'asc')
            ->find();
        return $this->response->setStatusCode(200)->setJSON($res);
    }

    public function get_data_grafik()
    {
        $res = [
            "data" => [
                'v1'        => [
                    'data' => [],
                ],
                'v2'        => [
                    'data' => [],
                ],
                'v3'        => [
                    'data' => [],
                ],
                'i1'        => [
                    'data' => [],
                ],
                'i2'        => [
                    'data' => [],
                ],
                'i3'        => [
                    'data' => [],
                ],
                'labels' => [],
            ],
            "last_id" => 0,
        ];

        $labels = [];
        $tmp = [];
        $tmp_data = [];

        $db = \Config\Database::connect();
        $ld_id = $this->request->getVar("ld_id");
        $last_id = $this->request->getVar("last_id");
        $type_get = $this->request->getVar("type_get");

        $limit = $type_get == 1 ? 10 : 1;
        $last_id_baru = 0;

        $where = "";
        $where .= " AND dd.ld_id = $ld_id ";

        if ($type_get == 1) {
            $data_label = $this->M_dashboard->get_label($where, $limit);
            if (count($data_label) > 0) {
                foreach ($data_label as $k => $v) {
                    $tgl =  date('d-m-Y H:i:s', strtotime($v->created_at));
                    $labels[] = $tgl;
                    $tmp[date("Y-m-d H:i:s", strtotime($tgl))] = $k;
                    $tmp_data[] = 0;

                    if ($k == count($data_label) - 1) $last_id_baru = $v->dd_id;
                }

                $res = [
                    "data" => [
                        'v1'        => [
                            'data' => $tmp_data,
                        ],
                        'v2'        => [
                            'data' => $tmp_data,
                        ],
                        'v3'        => [
                            'data' => $tmp_data,
                        ],
                        'i1'        => [
                            'data' => $tmp_data,
                        ],
                        'i2'        => [
                            'data' => $tmp_data,
                        ],
                        'i3'        => [
                            'data' => $tmp_data,
                        ],
                        'labels' => $labels,
                    ],
                    "last_id" => $last_id_baru,
                ];

                $data_sensor = $this->M_dashboard->get_sensor($where, $limit);
                foreach ($data_sensor as $k => $v) {
                    $res['data']['v1']['data'][$tmp[$v->created_at]] = floatval($v->v1);
                    $res['data']['v2']['data'][$tmp[$v->created_at]] = floatval($v->v2);
                    $res['data']['v3']['data'][$tmp[$v->created_at]] = floatval($v->v3);
                    $res['data']['i1']['data'][$tmp[$v->created_at]] = floatval($v->i1);
                    $res['data']['i2']['data'][$tmp[$v->created_at]] = floatval($v->i2);
                    $res['data']['i3']['data'][$tmp[$v->created_at]] = floatval($v->i3);
                }
            }
        } else {
            $where .= " AND dd.dd_id > $last_id ";
            $data_label = $this->M_dashboard->get_label($where, 1);
            if (count($data_label) > 0) {
                $labels = $data_label[0]->created_at;
                $res = [
                    "data" => [
                        'v1'        => [
                            'data' => 0,
                        ],
                        'v2'        => [
                            'data' => 0,
                        ],
                        'v3'        => [
                            'data' => 0,
                        ],
                        'i1'        => [
                            'data' => 0,
                        ],
                        'i2'        => [
                            'data' => 0,
                        ],
                        'i3'        => [
                            'data' => 0,
                        ],
                        'labels' => $labels,
                    ],
                    "last_id" => $data_label[0]->dd_id,
                ];

                $data_sensor = $this->M_dashboard->get_sensor($where, 1);
                foreach ($data_sensor as $k => $v) {
                    $res['data']['v1']['data'] = floatval($v->v1);
                    $res['data']['v2']['data'] = floatval($v->v2);
                    $res['data']['v3']['data'] = floatval($v->v3);
                    $res['data']['i1']['data'] = floatval($v->i1);
                    $res['data']['i2']['data'] = floatval($v->i2);
                    $res['data']['i3']['data'] = floatval($v->i3);
                }
            } else {
                $res = ['last_id' => $last_id];
            }
        }

        return $this->response->setStatusCode(200)->setJSON($res);
    }

    public function get_data()
    {
        $columns = array(
            "dd.dd_id",
            "dd.v1",
            "dd.v2",
            "dd.v3",
            "dd.timer",
            "dd.created_at",
            "dd.output",
            "k.reff_nama",
            "g.reff_nama",
            "dd.kwh",
        );

        $ld_id = $this->request->getVar('ld_id');
        $ld_id = !empty($ld_id) ? $ld_id : 0;
        $tgl_mulai = $this->request->getVar('tgl_mulai');
        $tgl_mulai = !empty($tgl_mulai) ? date('Y-m-d', strtotime($tgl_mulai)) : date('Y-m-d');
        $tgl_selesai = $this->request->getVar('tgl_selesai');
        $tgl_selesai = !empty($tgl_selesai) ? date('Y-m-d', strtotime($tgl_selesai)) : date('Y-m-d');
        $search = $this->request->getVar('search')['value'];
        $where = "";

        $where .= " AND DATE(dd.created_at) between '$tgl_mulai' and '$tgl_selesai' ";

        $where .= " AND dd.ld_id = $ld_id ";
        if (isset($search) && $search != "") {
            $where = "AND (";
            for ($i = 0; $i < count($columns); $i++) {
                $where .= " LOWER( cast( " . $columns[$i] . " as CHAR) ) LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $iTotalRecords = intval($this->M_dashboard->get_total($where));
        $length = intval($this->request->getVar('length'));
        $length = $length < 0 ? $iTotalRecords : $length;
        $start  = intval($this->request->getVar('start'));
        $draw      = intval($_REQUEST['draw']);
        $sortCol0 = $this->request->getVar('order')[0];
        $records = array();
        $records["data"] = array();
        $order = "";
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($start) . ", " . intval($length);
        }

        if (isset($sortCol0)) {
            $order = "ORDER BY  ";
            for ($i = 0; $i < count($this->request->getVar('order')); $i++) {
                if ($this->request->getVar('columns')[intval($this->request->getVar('order')[$i]['column'])]['orderable'] == "true") {
                    $order .= "" . $columns[intval($this->request->getVar('order')[$i]['column'])] . " " .
                        ($this->request->getVar('order')[$i]['dir'] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $order = substr_replace($order, "", -2);
            if ($order == "ORDER BY") {
                $order = "";
            }
        }
        $data = $this->M_dashboard->get_data($limit, $where, $order, $columns);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $records["data"][] = array(
                $no++,
                $row->v1,
                $row->v2,
                $row->v3,
                $row->timer,
                $row->created_at,
                $row->output,
                $row->kondisi,
                $row->gangguan,
                $row->kwh,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }
}
