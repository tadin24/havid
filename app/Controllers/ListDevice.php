<?php

namespace App\Controllers;

use App\Controllers\MyController;
use App\Models\ListDeviceModel;
use App\Models\MsDeviceModel;

class ListDevice extends MyController
{
    protected $M_list_device;
    protected $M_ms_device;
    public function __construct()
    {
        parent::__construct();
        $this->M_list_device = new ListDeviceModel();
        $this->M_ms_device = new MsDeviceModel();
    }

    public function index()
    {
        $data['opt_device'] = $this->M_ms_device->where('device_status', 1)->orderBy('device_kode', 'asc')->find();
        $data['title'] = "List Device";
        return $this->base_theme('v_list_device', $data);
    }

    public function get_data()
    {
        $columns = array(
            'ld.ld_id',
            'ld.ld_kode',
            'ld.ld_url',
            'ld.ld_status',
            'ld.device_id',
            'ld.user_id',
        );

        $colSearch = [
            'ld.ld_kode',
        ];

        $search = $this->request->getVar('search')['value'];
        $device_id = $this->request->getVar('fil_device_id');
        $user_id = $this->userdata->user_id;

        $where = "";

        if ($user_id != 1) {
            $where .= " AND ld.user_id = $user_id ";
        }

        if ($device_id) {
            $where .= " AND ld.device_id = $device_id ";
        }

        if (isset($search) && $search != "") {
            $where .= " AND (";
            for ($i = 0; $i < count($colSearch); $i++) {
                $where .= " LOWER(" . $colSearch[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ') ';
        }
        $iTotalRecords = intval($this->M_list_device->get_total($where));
        $length = intval($this->request->getVar('length'));
        $length = $length < 0 ? $iTotalRecords : $length;
        $start  = intval($this->request->getVar('start'));
        $draw      = intval($_REQUEST['draw']);
        $sortCol0 = $this->request->getVar('order')[0];
        $records = array();
        $records["data"] = array();
        $order = "";
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($start) . ", " . intval($length);
        }

        if (isset($sortCol0)) {
            $order = "ORDER BY  ";
            for ($i = 0; $i < count($this->request->getVar('order')); $i++) {
                if ($this->request->getVar('columns')[intval($this->request->getVar('order')[$i]['column'])]['orderable'] == "true") {
                    $order .= "" . $columns[intval($this->request->getVar('order')[$i]['column'])] . " " .
                        ($this->request->getVar('order')[$i]['dir'] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $order = substr_replace($order, "", -2);
            if ($order == "ORDER BY") {
                $order = "";
            }
        }
        $data = $this->M_list_device->get_data($limit, $where, $order, $columns);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $isi = rawurlencode(json_encode($row));

            if ($row->ld_status == 1) {
                $status = '<span class="badge rounded-pill badge-success">Aktif</span>';
            } else {
                $status = '<span class="badge rounded-pill badge-danger">Non Aktif</span>';
            }

            $action .= '<div class="d-grid gap-2 d-md-block">
                            <button onclick="set_val(\'' . $isi . '\')" class="btn btn-sm btn-primary" title="Edit">
                                <i class="fa fa-pencil-alt"></i>
                            </button>
                            <button onclick="set_del(\'' . $row->ld_id . '\')" class="btn btn-sm btn-danger " title="Delete">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>';

            $records["data"][] = array(
                $no++,
                $row->ld_kode,
                $row->ld_url,
                $status,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function save()
    {
        $act = addslashes($this->request->getVar('act'));
        $data = [
            'device_id' => addslashes($this->request->getVar('device_id')),
            'ld_kode' => addslashes($this->request->getVar('ld_kode')),
            'ld_url' => addslashes($this->request->getVar('ld_url')),
            'ld_status' => $this->request->getVar('ld_status'),
        ];

        if ($act == 'add') {
            $data['user_id'] = $this->userdata->user_id;
            $res = $this->M_list_device->insert($data);
        } else {
            $data['user_id'] = $this->request->getVar('user_id');
            $id = $this->request->getVar('ld_id');
            $res = $this->M_list_device->update($id, $data);
        }

        if ($res > 0) {
            $response = [
                'status' => true,
                'message' => $act == 'add' ? 'Berhasil menambahkan data!' : 'Berhasil memperbarui data!',
                'title' => 'Success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' =>  $act == 'add' ? 'Gagal menambahkan data!' : 'Gagal memperbarui data!',
                'title' => 'Error',
            ];
        }

        echo json_encode($response);
    }

    public function hapus($id)
    {
        $res = $this->M_list_device->delete($id);

        $response = [
            'status' => false,
            'message' => "Data Gagal dihapus"
        ];

        if ($res) {
            $response = [
                'status' => true,
                'message' => "Data Berhasil dihapus"
            ];
        }

        echo json_encode($response);
    }
}
