<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MsReferensiSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'reff_id'       => 1,
                'reff_kode'     => '01',
                'reff_nama'     => 'Normal',
                'reff_status'   => 1,
                'reff_value'    => 1,
                'refcat_id'     => 1,
            ], [
                'reff_id'       => 2,
                'reff_kode'     => '02',
                'reff_nama'     => 'Interuption',
                'reff_status'   => 1,
                'reff_value'    => 2,
                'refcat_id'     => 1,
            ], [
                'reff_id'       => 3,
                'reff_kode'     => '03',
                'reff_nama'     => 'Sustained Interuption',
                'reff_status'   => 1,
                'reff_value'    => 3,
                'refcat_id'     => 1,
            ], [
                'reff_id'       => 4,
                'reff_kode'     => '04',
                'reff_nama'     => 'Sag',
                'reff_status'   => 1,
                'reff_value'    => 4,
                'refcat_id'     => 1,
            ], [
                'reff_id'       => 5,
                'reff_kode'     => '05',
                'reff_nama'     => 'Undervoltage',
                'reff_status'   => 1,
                'reff_value'    => 5,
                'refcat_id'     => 1,
            ], [
                'reff_id'       => 6,
                'reff_kode'     => '06',
                'reff_nama'     => 'Swell',
                'reff_status'   => 1,
                'reff_value'    => 6,
                'refcat_id'     => 1,
            ], [
                'reff_id'       => 7,
                'reff_kode'     => '07',
                'reff_nama'     => 'Overvoltage',
                'reff_status'   => 1,
                'reff_value'    => 7,
                'refcat_id'     => 1,
            ], [
                'reff_id'       => 8,
                'reff_kode'     => '01',
                'reff_nama'     => 'Gangguan Relay 1',
                'reff_status'   => 1,
                'reff_value'    => 'A',
                'refcat_id'     => 2,
            ], [
                'reff_id'       => 9,
                'reff_kode'     => '02',
                'reff_nama'     => 'Gangguan Relay 2',
                'reff_status'   => 1,
                'reff_value'    => 'B',
                'refcat_id'     => 2,
            ], [
                'reff_id'       => 10,
                'reff_kode'     => '03',
                'reff_nama'     => 'Gangguan Relay 3',
                'reff_status'   => 1,
                'reff_value'    => 'C',
                'refcat_id'     => 2,
            ], [
                'reff_id'       => 11,
                'reff_kode'     => '04',
                'reff_nama'     => 'Gangguan Relay 4',
                'reff_status'   => 1,
                'reff_value'    => 'D',
                'refcat_id'     => 2,
            ],
        ];

        $this->db->table("ms_reff")->insertBatch($data);
    }
}
